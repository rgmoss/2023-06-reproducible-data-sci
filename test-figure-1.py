import matplotlib.pyplot as plt

# Create a (rather uninteresting) figure.
plt.plot([1, 2, 3])
# And save it as a PDF.
plt.savefig('test.pdf')
