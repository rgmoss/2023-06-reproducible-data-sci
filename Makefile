TEX := xelatex
TEX_FLAGS := -interaction=batchmode -shell-escape

DOC := moss-reproducible-data-science
SLIDES := $(DOC).pdf

TMP_EXTS := aux log nav out snm toc vrb
TMP_FILES := $(foreach EXT,$(TMP_EXTS),$(wildcard *.$(EXT)))
TMP_FILES += $(wildcard _minted-*)


default: $(SLIDES)

%.pdf: %.tex $(wildcard figures/*.pdf)
	@$(TEX) $(TEX_FLAGS) $*
	@$(TEX) $(TEX_FLAGS) $*

clean:
	@rm -rf $(TMP_FILES)

distclean: clean
	@rm -f $(SLIDES)

.PHONY: default clean distclean
