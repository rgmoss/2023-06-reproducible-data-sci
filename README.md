# Reproducible data science: what and why?

I prepared this talk for the Master of Data Science cohort on Thursday 27 July, 2023.

See the [PDF version of the slides](moss-reproducible-data-science.pdf).
