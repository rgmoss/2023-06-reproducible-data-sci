\documentclass[12pt,aspectratio=169,presentation]{beamer}

%% My rgm-plain Beamer theme, available from:
%% https://bitbucket.org/robmoss/beamer-theme-rgm-plain/
\usetheme[emph,heavy]{rgm-plain}
\usecolortheme{base16-tomorrow}
\setsansfont[Mapping=tex-text,Numbers={Lining}]{Open Sans}
\setmainfont[Mapping=tex-text,Numbers={Lining}]{Open Sans}

%% Talk title and front matter.
\date{Thursday 27 July, 2023}
\title{Reproducible data science: what and why?}
\author{Rob Moss\\[1.25em]{\small Melbourne School of Population and Global Health\\The University of Melbourne}}
\institute{\usebeamercolor[fg]{title}\small \hfill{}rgmoss@unimelb.edu.au\hfill{}@rob\_models@mas.to\hfill{}\break}

%% Minor adjustments to frame titles, alert text, and speaking notes.
\setbeamertemplate{frametitle}[default][center]
\setbeamerfont{alerted text}{series=\bfseries}
\setbeamercolor{alerted text}{use=normal text,fg=normal text.fg}
\setbeamercolor{note page}{bg=white, fg=black}
\setbeamercolor{note title}{bg=white, fg=black}
\setbeamercolor{note date}{bg=white, fg=white}

%% Support packages for figures, annotations, and hyperlinks.
\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{tikz}
\usepackage{hyperref}
\hypersetup{colorlinks,linkcolor=,urlcolor=blue}

%% Define a custom colour for faded text.
\colorlet{faded}{black!30!white}

\usepackage{minted}
%% \usemintedstyle{friendly}
\usemintedstyle{lovelace}

\begin{document}

%% Define custom lengths for separating list items.
\newlength{\myitemsep}
\setlength{\myitemsep}{1em}
\newlength{\mybigsep}
\setlength{\mybigsep}{2em}

%% Use a heavier font for URLs.
\def\UrlFont{\ttfamily\bfseries}

\begin{frame}{}
\maketitle
\end{frame}

\begin{frame}{Background: what I do}
  \begin{columns}[T]
    \begin{column}{0.6\textwidth}
      My research:\vskip0.5em
      \begin{itemize}
      \item Informing national pandemic plans\\[0.5em]
      \item Near-real-time outbreak forecasting\\[0.75em]
      \end{itemize}
      \vskip\baselineskip
      \pause

      I develop:
      \begin{itemize}
      \item Infectious disease simulation models\\[0.5em]
      \item Bayesian inference methods\\[0.5em]
      \end{itemize}
      \vphantom{1em}
    \end{column}
    \begin{column}{0.4\textwidth}
      \centering
      \includegraphics<2>[width=\textwidth]{nsw-2016-fs-anatomy}%
      \includegraphics<3->[width=\textwidth]{pm}

      \only<2>{\scriptsize%
        \href{https://doi.org/10.33321/cdi.2019.43.7}{doi:10.33321/cdi.2019.43.7}}
    \end{column}
  \end{columns}
  \vskip1.5\baselineskip

  \centering
  \uncover<3>{
  Models + Data \(\rightarrow\) Decision \(\Leftrightarrow\) Data science
  }
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture,overlay,shift={(current page.center)}]
    \node[at=(current page.center)]{
      \includegraphics[width=\paperwidth, height=\paperheight]{insanity-quote-einstein}
    };
    \draw<2>[red,ultra thick,rounded corners] (-6.35,1.2) rectangle (-2.6,0.4);
    \draw<2>[red,ultra thick,rounded corners] (-6.35,-0.45) rectangle (-1.05,-1.2);
    \draw<3>[red,ultra thick,rounded corners] (-6.25,-1.5) rectangle (-3.1,-2.1);
  \end{tikzpicture}
\end{frame}

\begin{frame}{Can you do ``the same thing''?}
  \begin{center}
    \only<1>{%
      \includegraphics[height=0.8\textheight]{draw-an-owl-0}%
    }\only<2>{%
      \includegraphics[height=0.8\textheight]{draw-an-owl-1}%
    }
  \end{center}
\end{frame}

\begin{frame}{This can be surprisingly difficult}
  \begin{center}
    \begin{minipage}{0.5\textwidth}
      You might think that if you:\vskip0.5em
      \pause
      \begin{itemize}
      \item write a program,\\[0.5em] \pause
      \item and don't change anything,\\[0.5em] \pause
      \item then come back a day later,\\[0.5em] \pause
      \item (or a decade later),\\[0.5em] \pause
      \item and run it with the same inputs\\[0.75em] \pause
      \end{itemize}
      you would get the same output. \pause
    \end{minipage}
    \vskip\baselineskip
    \textbf{But the output can still change!}
    \vskip0.5\baselineskip
    \url{http://www.tdda.info/why-code-rusts}
  \end{center}
\end{frame}

\begin{frame}{Data science and trust}
  \centering
  \textcolor<2->{faded}{%
  Data science is increasingly informing decisions \textbf{that affect us all}.}
  \vspace{2\baselineskip}

  \textcolor<1,3->{faded}{%
  We need to \textbf{trust the results} in order to justify the decisions.}
  \vspace{2\baselineskip}

  \textcolor<-2>{faded}{%
  How can we have trust if we cannot \textbf{reproduce the results}?}
\end{frame}

\begin{frame}{Code has value beyond its immediate outputs}
  \centering
  \begin{quote}
    "Code is the life-blood of research in our field. In order to truly build on the discoveries and advances in computational biology, we must be able to build on the work of others."
    \vspace{\baselineskip}

    \scriptsize
    {\em Alison Mudditt}, \href{https://theplosblog.plos.org/2023/06/open-science-takes-people-willing-to-disrupt-the-system-to-move-it-forward/}{Open Science takes people willing to disrupt the system to move it forward}.\\
    {\em The Official PLOS Blog, 12 June 2023.}
  \end{quote}
\end{frame}

\begin{frame}{Example: COVID-19 in Australia (2020--)}
  \centering
  \includegraphics[width=0.8\textwidth]{vic-fs-second-wave}
  \vspace{0.25\baselineskip}

  {\footnotesize Moss et al., 2023, \href{https://doi.org/10.1038/s41598-023-35668-6}{doi:10.1038/s41598-023-35668-6}}
\end{frame}

\begin{frame}{Example: COVID-19 in Australia (2020--)}
  \textcolor<2->{faded}{%
  Many substantial, rapid changes to model structure and input data.}
  \vspace{2\baselineskip}

  \textcolor<1,3->{faded}{%
  Past experiences \textbf{strongly suggest} I will make mistakes.}
  \vspace{2\baselineskip}

  \textcolor<-2,4->{faded}{%
  So I wrote \textbf{lots} of validation experiments and test cases.}
  \vspace{2\baselineskip}

  \textcolor<-3>{faded}{%
  ≈12,000 lines of code and ≈6,000 lines of tests (all publicly available).}
\end{frame}

\begin{frame}{Reproducibility is a gradual scale}
  \centering
  \includegraphics[width=0.8\textwidth]{scale}
\end{frame}

\begin{frame}{Reproducibility is cultural}
  \textcolor<2->{faded}{%
  It is a process of learning and adoption, rather than a destination.}
  \vspace{2\baselineskip}

  \textcolor<1,3->{faded}{%
  It is just as much about \textbf{simple work habits} as it is about tools.}
  \vspace{2\baselineskip}

  \textcolor<-2,4->{faded}{%
  The challenge can often be \textbf{how to implement practices}.}
  \vspace{2\baselineskip}

  \textcolor<-3>{faded}{%
  This may require support from colleagues, supervisors, etc.}
\end{frame}

\begin{frame}{Reproducibility is valuable}
  \centering

  \textcolor<2->{faded}{%
    Remember \textbf{what} you did, \textbf{how} you did it, and \textbf{why}!}
  \vspace{2\baselineskip}

  \textcolor<1,3->{faded}{%
    Allows you to reuse, adapt, and share \textbf{with confidence}.}
  \vspace{2\baselineskip}

  \textcolor<-2>{faded}{%
    Found a mistake? What past results did it affect?}
\end{frame}

{
  \setbeamercolor{background canvas}{bg=black}
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture,overlay,shift={(current page.center)}]
      \node[at=(current page.center)]{
        \includegraphics[height=\paperheight]{i-know}
      };
    \end{tikzpicture}
  \end{frame}
}

\begin{frame}{Reproducibility: going back in time}
  \textcolor<2->{faded}{%
  A colleague notified us of a likely error in a figure in a pre-print article.}
  \vspace{2\baselineskip}

  \textcolor<1,3->{faded}{%
  I confirmed there was an error in the plotting script.}
  \vspace{2\baselineskip}

  \textcolor<-2,4->{faded}{%
  We were able to verify that no other outputs were affected.}
  \vspace{2\baselineskip}

  \textcolor<-3>{faded}{%
  Importantly, no reports to government were affected.}
  \vskip2\baselineskip
  \scriptsize\url{https://git-is-my-lab-book.net/case-studies/moss-incorrect-data-pre-print.html}
\end{frame}

\begin{frame}{Reproducibility can be hard}
  \centering
  \begin{minipage}{0.7\textwidth}
    There are many complicating factors, such as:
    \vspace{\baselineskip}

    \begin{itemize}
    \item Build environments\\[0.5em]
    \item Software versions and updates\\[0.5em]
    \item File metadata (timestamps, paths, etc)\\[0.5em]
    \item Randomness and stochastic effects
    \end{itemize}
    \vspace{\baselineskip}

    See \url{https://reproducible-builds.org/} for more.
  \end{minipage}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Reproducible images and metadata (1)}

  \begin{center}
    \begin{minipage}{0.7\textwidth}
      \inputminted{python}{test-figure-1.py}
      \vspace{\baselineskip}
      \pause

      \textbf{But it isn't reproducible:}
      \vspace{\baselineskip}

      \begin{minted}[autogobble]{shell-session}
        # python3 test-figure-1.py && shasum test.pdf
        44e360d0a851f0891a9ff06a3c7b752a5ac62535  test.pdf
        # python3 test-figure-1.py && shasum test.pdf
        77263d91a04342e9eacc37fe48d0ea16e0e842d6  test.pdf
      \end{minted}
    \end{minipage}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Reproducible images and metadata (2)}

  \begin{center}
    \begin{minipage}{0.7\textwidth}
      \inputminted{python}{test-figure-2.py}
      \vspace{\baselineskip}
      \pause

      \textbf{Now it is reproducible:}
      \vspace{\baselineskip}

      \begin{minted}[autogobble]{shell-session}
        # python3 test-figure-2.py && shasum test.pdf
        3f669fc3893c9ed2bbe09146f885966d4ea2fc52  test.pdf
        # python3 test-figure-2.py && shasum test.pdf
        3f669fc3893c9ed2bbe09146f885966d4ea2fc52  test.pdf
      \end{minted}
    \end{minipage}
  \end{center}
\end{frame}

\begin{frame}{Reproducible images and metadata (3)}
  \centering
  \includegraphics[width=0.6\textwidth]{test}
\end{frame}

\begin{frame}{Reproducibility can be easy}
  \begin{minipage}{0.95\textwidth}
    \centering
    \begin{itemize}
    \item Use version control — commit \textbf{early} and \textbf{often}!\\[1em]
    \item Write test cases — what \textbf{should} your code do?\\[1em]
    \item Automate workflows — limit opportunities for \textbf{human error}.\\[1em]
    \item Use continuous integration — get \textbf{rapid feedback}!
    \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \centering
  \vfill
  {\usebeamerfont*{frametitle}\usebeamercolor[fg]{frametitle} Thank you! Questions?}
  \vfill
\end{frame}

\begin{frame}{} %%{Useful resources}
  Great review articles:
  \begin{itemize}
  \item \href{https://doi.org/10.1002/bes2.1801}{A Beginner's Guide to Conducting Reproducible Research}
  \item \href{https://doi.org/10.1098/rspb.2022.1113}{Why don't we share data and code? Perceived barriers and benefits to public archiving practices}
  \end{itemize}
  \vspace{\baselineskip}

  Helpful resources:
  \begin{itemize}
  \item \href{http://www.tdda.info/why-code-rusts}{Why Code Rusts}
  \item \href{https://git-is-my-lab-book.net/}{Git is my lab book} (I'm a lead author)
  \item \href{http://www.practicereproducibleresearch.org}{The Practice of Reproducible Research}
  \item \href{https://the-turing-way.netlify.app/}{The Turing Way}
  \item \href{https://carpentries.org/}{The Carpentries}
  \end{itemize}
  \vspace{\baselineskip}

  {\small
    \url{https://gitlab.unimelb.edu.au/rgmoss/2023-06-reproducible-data-sci}}
\end{frame}

\end{document}
